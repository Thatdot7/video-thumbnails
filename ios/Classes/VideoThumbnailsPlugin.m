#import "VideoThumbnailsPlugin.h"
#import <video_thumbnails/video_thumbnails-Swift.h>

@implementation VideoThumbnailsPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftVideoThumbnailsPlugin registerWithRegistrar:registrar];
}
@end
