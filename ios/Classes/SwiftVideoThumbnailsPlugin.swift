import Flutter
import UIKit
import AVFoundation

public class SwiftVideoThumbnailsPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "video_thumbnail", binaryMessenger: registrar.messenger())
    let instance = SwiftVideoThumbnailsPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    if (call.method == "getPlatformVersion")
    {
      result("iOS " + UIDevice.current.systemVersion)
    }
    else if (call.method == "getThumbnail")
    {
      
      let dispatchQueue = DispatchQueue(label: "ThumbnailRetriever", qos: .utility);
      dispatchQueue.async {
        let argumentList = call.arguments as! Dictionary<String, String>;
        let word = argumentList["path"]! as String;
          
        let url = URL(fileURLWithPath: word, isDirectory: false)

        // ==================================
        // Everything below this line should not be touched
        let asset = AVAsset(url: url)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(Float64(1), preferredTimescale: 100)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let imageData = UIImage(cgImage: img).pngData()
            result(imageData)
        } catch {
            result(nil)
        }
      }
    }
    else
    {
      result(FlutterMethodNotImplemented);
    }
  }

}
