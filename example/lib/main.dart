import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_thumbnails/video_thumbnails.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

  }

  Future<ImageProvider> retrieveThumbnail() async {
    String trialpath;

    if(Platform.isAndroid) {
      Directory extPath = await getExternalStorageDirectory();
      trialpath = extPath.path + "/Download/test.mp4";
    } else if (Platform.isIOS) {
      Directory extPath = await getApplicationDocumentsDirectory();
      trialpath = extPath.path + "/test.mp4";
    }
    print(trialpath);
    ImageProvider image = await VideoThumbnail.getThumbnail(trialpath);
    return image;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: FutureBuilder(
            future: retrieveThumbnail(),
            builder: (context, snapshot) {
              if(snapshot.hasData && snapshot.data != null)
                return Image(image:snapshot.data,
                  width: 64.0
                );
              else
                return CircularProgressIndicator();
            }
          ),
        ),
      ),
    );
  }

}
