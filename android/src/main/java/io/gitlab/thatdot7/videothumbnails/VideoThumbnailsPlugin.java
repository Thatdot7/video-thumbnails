package io.gitlab.thatdot7.videothumbnails;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/** VideoThumbnailPlugin */
public class VideoThumbnailsPlugin implements MethodCallHandler {
  /**
   * Plugin registration.
   */

  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "video_thumbnail");
    channel.setMethodCallHandler(new VideoThumbnailsPlugin());
  }

  @Override
  public void onMethodCall(MethodCall call, Result result) {

    // This is where the custom methods will be called from dart.
    if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);
    } else if (call.method.equals("getThumbnail")) {
      String videoPath = call.argument("path");
      try {
        byte[] data = new ThumbnailRetrieverTask().execute(videoPath).get();
        if (data == null)
          result.error("Error", "File not found or video format not supported", null);
        else
          result.success(data);
      } catch (InterruptedException e) {
        e.printStackTrace();
        result.error("Thumbnail creation interrupted", null, null);
      } catch (ExecutionException e) {
        e.printStackTrace();
        result.error("Thumbnail creation failed", null, null);
      }


    } else {
      result.notImplemented();
    }
  }

  class ThumbnailRetrieverTask extends AsyncTask<String,Integer,byte[]> {

    @Override
    protected byte[] doInBackground(String... strings) {
      File videoFile = new File(strings[0]);
      Log.d("VideoThumbnailPlugin", "videoExists?: " + videoFile.exists() + "; videoPath: " + videoFile.getAbsolutePath());
      Bitmap thumb = ThumbnailUtils.createVideoThumbnail(videoFile.getAbsolutePath(),MediaStore.Images.Thumbnails.MICRO_KIND);

      if (thumb == null) {
        Log.e("VideoThumbnailPlugin", "createVideoThumbnail is null");
        return null;
      }

      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      thumb.compress(Bitmap.CompressFormat.PNG, 100, stream);
      byte[] byteArray = stream.toByteArray();
      thumb.recycle();
      return byteArray;
    }
  }

}
