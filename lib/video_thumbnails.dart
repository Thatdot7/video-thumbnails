import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';



class VideoThumbnail {
  static const MethodChannel _channel =
      const MethodChannel('video_thumbnail');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<ImageProvider> getThumbnail(String path) async {

    var completer = new Completer<ImageProvider>();
    var version = await platformVersion;

    if(version.contains("droid")){
      var permissionGranted = await PermissionHandler().requestPermissions([PermissionGroup.storage]);


      if(permissionGranted[PermissionGroup.storage] == PermissionStatus.granted) {
        final List<dynamic> byteData = await _channel.invokeMethod('getThumbnail', {"path": path});
        final MemoryImage image = new MemoryImage(Uint8List.fromList(byteData));
        completer.complete(image);
      } else {
        completer.complete(null);
      }
    } else {
      final List<dynamic> byteData = await _channel.invokeMethod('getThumbnail', {"path": path});
      final MemoryImage image = new MemoryImage(Uint8List.fromList(byteData));
      completer.complete(image);
    }
    return completer.future;
  }

  static Future<String> getThumbnailInBase64(String path) async {

    var completer = new Completer<String>();
    var version = await platformVersion;

    if(version.contains("droid")){
      var permissionGranted = await PermissionHandler().requestPermissions([PermissionGroup.storage]);


      if(permissionGranted[PermissionGroup.storage] == PermissionStatus.granted) {
        final List<dynamic> byteData = await _channel.invokeMethod('getThumbnail', {"path": path});
        completer.complete(base64Encode(byteData));
      } else {
        completer.complete(null);
      }
    } else {
      final List<dynamic> byteData = await _channel.invokeMethod('getThumbnail', {"path": path});
      completer.complete(base64Encode(byteData));
    }
    return completer.future;
  }

  static Future<Uint8List> getThumbnailInUint8List(String path) async {

    var completer = new Completer<Uint8List>();
    var version = await platformVersion;

    var file = File(path);
    if(!file.existsSync()) {
      print("File doesn't exists");
      completer.complete(null);
    }
      

    if(version.contains("droid")){
      var permissionGranted = await PermissionHandler().requestPermissions([PermissionGroup.storage]);


      if(permissionGranted[PermissionGroup.storage] == PermissionStatus.granted) {
        final List<dynamic> byteData = await _channel.invokeMethod('getThumbnail', {"path": path});
        completer.complete(Uint8List.fromList(byteData));
      } else {
        completer.complete(null);
      }
    } else {
      final List<dynamic> byteData = await _channel.invokeMethod('getThumbnail', {"path": path});
      completer.complete(Uint8List.fromList(byteData));
    }
    return completer.future;
  }
}
